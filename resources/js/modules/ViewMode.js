class ViewMode {
    get selectElement() {
        return document.querySelector('[data-select-mode]');
    }

    get japaneseContent() {
        return document.getElementById('japanese-lyrics');
    }

    get romajiContent() {
        return document.getElementById('romaji-lyrics');
    }

    init() {
        this.attachListeners();
    }

    /**
     * Attach listener to select[data-select-mode] when available.
     * When the value is changed, it will change which lyric mode is shown.
     *
     * @returns void
     * @memberof ViewMode
     */
    attachListeners() {
        const target = this.selectElement;
        if (target !== null) {
            target.addEventListener('change', e => {
                this.changeViewMode(e.target.value);
            });
        } else {
            console.warn('Cannot locate `data-select-mode` target');
        }
    }

    changeViewMode(value) {
        this.hideContent('japanese');
        this.hideContent('romaji');

        this.showContent(value);
    }

    showContent(mode) {
        let target;

        if (mode === 'japanese') {
            target = this.japaneseContent;
        } else if (mode === 'romaji') {
            target = this.romajiContent;
        }

        if (target instanceof HTMLElement) {
            target.style.display = 'block';
        }
    }

    hideContent(mode) {
        let target;

        if (mode === 'japanese') {
            target = this.japaneseContent;
        } else if (mode === 'romaji') {
            target = this.romajiContent;
        }

        if (target instanceof HTMLElement) {
            target.style.display = 'none';
        }
    }
}

const viewMode = new ViewMode();
viewMode.init();
