<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="https://kit.fontawesome.com/3269cecf45.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,900&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>


</head>

<body>
    <!--[if lt IE 7]>
                <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

    <header>
        <div class="flex-center position-ref">

            <!-- Navigation Menu -->
            <nav>
                <div>
                    <a href="{{ url('/') }}">Allup Lyrics</a>

                    <a href="#" class="icon" data-toggle-menu>
                        <i class="fa fa-bars"></i>
                    </a>
                </div>

                <ul id="myLinks" style="display: none;">
                    @if (Route::has('login'))
                    <li>
                        @auth
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    <li>
                        <a href="#">Lyrics <i class="fa fa-caret-down"></i></a>
                        <ul>
                            <li>
                                <a href="{{ route('lyrics.index', ['letter' => 'a']) }}">A</a>
                            </li>
                            <li>
                                <a href="#">B</a>
                            </li>
                            <li>
                                <a href="#">C</a>
                            </li>
                            <li>
                                <a href="#">D</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">F</a>
                            </li>
                            <li>
                                <a href="#">G</a>
                            </li>
                            <li>
                                <a href="#">H</a>
                            </li>
                            <li>
                                <a href="#">I</a>
                            </li>
                            <li>
                                <a href="#">J</a>
                            </li>
                            <li>
                                <a href="#">K</a>
                            </li>
                            <li>
                                <a href="#">L</a>
                            </li>
                            <li>
                                <a href="#">M</a>
                            </li>
                            <li>
                                <a href="#">N</a>
                            </li>
                            <li>
                                <a href="#">O</a>
                            </li>
                            <li>
                                <a href="#">P</a>
                            </li>
                            <li>
                                <a href="#">Q</a>
                            </li>
                            <li>
                                <a href="#">R</a>
                            </li>
                            <li>
                                <a href="#">S</a>
                            </li>
                            <li>
                                <a href="#">T</a>
                            </li>
                            <li>
                                <a href="#">U</a>
                            </li>
                            <li>
                                <a href="#">V</a>
                            </li>
                            <li>
                                <a href="#">W</a>
                            </li>
                            <li>
                                <a href="#">X</a>
                            </li>
                            <li>
                                <a href="#">Y</a>
                            </li>
                            <li>
                                <a href="#">Z</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                    @else
                    <li>
                        <a href="{{ url('/') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ url('/about') }}">About</a>
                    </li>
                    <li>
                        <a href="#">Lyrics <i class="fa fa-caret-down"></i></a>
                        <ul>
                            <li>
                                <a href="#">#</a>
                            </li>
                            <li>
                                <a href="#">A</a>
                            </li>
                            <li>
                                <a href="#">B</a>
                            </li>
                            <li>
                                <a href="#">C</a>
                            </li>
                            <li>
                                <a href="#">D</a>
                            </li>
                            <li>
                                <a href="#">E</a>
                            </li>
                            <li>
                                <a href="#">F</a>
                            </li>
                            <li>
                                <a href="#">G</a>
                            </li>
                            <li>
                                <a href="#">H</a>
                            </li>
                            <li>
                                <a href="#">I</a>
                            </li>
                            <li>
                                <a href="#">J</a>
                            </li>
                            <li>
                                <a href="#">K</a>
                            </li>
                            <li>
                                <a href="#">L</a>
                            </li>
                            <li>
                                <a href="#">M</a>
                            </li>
                            <li>
                                <a href="#">N</a>
                            </li>
                            <li>
                                <a href="#">O</a>
                            </li>
                            <li>
                                <a href="#">P</a>
                            </li>
                            <li>
                                <a href="#">Q</a>
                            </li>
                            <li>
                                <a href="#">R</a>
                            </li>
                            <li>
                                <a href="#">S</a>
                            </li>
                            <li>
                                <a href="#">T</a>
                            </li>
                            <li>
                                <a href="#">U</a>
                            </li>
                            <li>
                                <a href="#">V</a>
                            </li>
                            <li>
                                <a href="#">W</a>
                            </li>
                            <li>
                                <a href="#">X</a>
                            </li>
                            <li>
                                <a href="#">Y</a>
                            </li>
                            <li>
                                <a href="#">Z</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                        <a href="{{ route('login') }}">Login</a>
                    </li>
                    <li>
                        @if (Route::has('register'))
                        <a href="{{ route('register') }}">Register</a>
                        @endif
                        @endauth
                        @endif
                    </li>
                </ul>
            </nav>
        </div>
    </header>

    @yield('content')

    <footer class="footer_container ">
        <span class="text-xl">©2020 Allup Lyrics || All Rights Reserved</span><br />
        <span class="text-base">Developed by: Rafael de Oliveira Silva</span>
        <script src="{{ asset('/js/scripts.js') }}" defer></script>
    </footer>
</body>

</html>