@extends('adminlte::page')

@section('plugins.Chartjs', true)

@section('title', 'Painel de Controle')
@section('content_header')
<div class="row">
   <div class="col-md-6">
      <h1>Dashboard</h1>
   </div>

   <div class="col-md-6">
      <form action="" method="get">
         <select name="interval" id="interval" class="float-md-right" onchange="this.form.submit()">
            <option {{$dateInterval==30?'selected="selected"':''}} value="30">Últimos 30 dias</option>
            <option {{$dateInterval==60?'selected="selected"':''}} value="60">Últimos 60 dias</option>
            <option {{$dateInterval==90?'selected="selected"':''}} value="90">Últimos 90 dias</option>
            <option {{$dateInterval==120?'selected="selected"':''}} value="120">Últimos 120 dias</option>
         </select>
      </form>
   </div>
</div>
@endsection

@section('content')

<div class="row">
   <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
         <div class="inner">
            <h3>{{ $visitsCount}}</h3>

            <p>Acessos</p>
         </div>
         <div class="icon">
            <i class="far fa-fw fa-eye"></i>
         </div>
         <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
         <div class="inner">
            <h3>{{$onlineCount}}</h3>

            <p>Usuários Online</p>
         </div>
         <div class="icon">
            <i class="fas fa-users"></i>
         </div>
         <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
         <div class="inner">
            <h3>{{$userCount}}</h3>

            <p>Novos Usuários</p>
         </div>
         <div class="icon">
            <i class="fas fa-user-plus"></i>
         </div>
         <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
   <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
         <div class="inner">
            <h3>{{$lyricsCount}}</h3>

            <p>Músicas Adicionadas</p>
         </div>
         <div class="icon">
            <i class="fas fa-music"></i>
         </div>
         <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
   </div>
   <!-- ./col -->
</div>

<div class="row">
   <div class="col-md-6">
      <div class="card">
         <div class="card-header">
            <h3 class="card-title">Páginas mais visitadas</h3>
            <div class="card-body">
               <canvas id="pagePie"></canvas>
            </div>
         </div>
      </div>
   </div>

   <div class="col-md-6">
      <div class="card">
         <div class="card-header">
            <h3 class="card-title">Sobre o Sistema</h3>
            <div class="card-body">
               <canvas id="pagePie"></canvas>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   window.onload = function() {
        let context = document.getElementById('pagePie').getContext('2d');
        window.pagePie = new Chart(context, {
            type:'pie',
            data:{
                datasets:[{
                    data: {{$pageValues}},
                    backgroundColor: '#0000FF'
                }],
                labels: {!! $pageLabels !!},
            },
            options:{
                responsive:true,
                legend:{
                    display:false
                }
            }
        });
    }
</script>
@endsection