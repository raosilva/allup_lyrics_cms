@extends('adminlte::page')

@section('title', 'Músicas')

@section('content_header')
<h1>
    <a href="{{ route('authlyrics.index') }}" class="btn btn-sm btn-info">Voltar à lista</a>
</h1>
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            {{$lyric->title}}
                        </h3>
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">Nome do cantor(a)/banda</h4><br><br>
                        <h6 class="card-subtitle text-muted">{{ $lyric->info }}</h6>
                    </div>
                    <img src="holder.js/100x180/" alt="">
                    <div class="card-body">
                        <div class="embeddedVideo">
                            <iframe src="{{$lyric->video_url}}" frameborder="0"></iframe>
                        </div><br><br>
                        <p class="lyrics-text" style="white-space: pre-line;">
                            {!!$lyric->lyric!!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('css')
<link rel="stylesheet" href="/css/admin-styles.css">
@endsection