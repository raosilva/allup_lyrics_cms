@extends('adminlte::page')

@section('title', 'Usuários')

@section('content_header')
<h1>Meus Usuários
    <a href="{{ route('users.create') }}" class="btn btn-sm btn-success ml-3 text-uppercase">Adicionar
        Usuário</a>
</h1>
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th>Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($users as $user)
                <tr>
                    <th>{{ $user->id }}</th>
                    <th>{{ $user->name }}</th>
                    <th>{{ $user->email }}</th>
                    <th>
                        <a href="{{ route('users.edit', ['user' => $user->id]) }}"
                            class="btn btn-sm btn-info">Editar</a>

                        @if ($loggedId !== intval($user->id))
                        <form action="{{ route('users.destroy', ['user' => $user->id]) }}" method="POST"
                            class="d-inline" onsubmit="return confirm('Tem certeza que deseja excluir este usuário?')">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Excluir" class="btn btn-sm btn-danger">
                            @endif
                        </form>
                    </th>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

{{$users->links()}}

@endsection