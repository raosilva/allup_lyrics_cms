@extends('adminlte::page')

@section('title', 'Cadastrar Música')

@section('content_header')
<h1>Nova Música</h1>
@endsection

@section('content')

@if($errors->any())
<div class="alert alert-danger">
    <h5> <i class="icon fa s fa-ban"></i> Alerta! </h5>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="card">
    <div class="card-body">
        <form class="form-horizontal" action="{{ route('lyrics.update', ['lyric'=>$lyric->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Título</label>
                <div class="col-sm-10">
                    <input type="text" name="title" value="{{$lyric->title}}"
                        class="form-control @error('title') is-invalid @enderror" />
                </div>
            </div>

            {{-- <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Artist</label>
                        <div class="col-sm-10">
                            <input type="text" name="artist" value="{{$lyric->artist}}" class="form-control
            @error('artist') is-invalid @enderror" />
    </div>
</div> --}}

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Extra Info</label>
    <div class="col-sm-10">
        <input type="text" name="info" value="{{$lyric->info}}" class="form-control" />
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">URL da Música</label>
    <div class="col-sm-10">
        <input type="url" name="video_url" value="{{$lyric->video_url}}"
            class="form-control @error('video_url') is-invalid @enderror" />
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label">Letra da Música</label>
    <div class="col-sm-10">
        <textarea name="lyric" id="lyric" row="15"
            class="form-control bodyfield @error('lyric') is-invalid @enderror">{{$lyric->lyric}}</textarea>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label"></label>
    <div class="col-sm-10">
        <input type="submit" value="Salvar" class="btn btn-info" />
    </div>
</div>
</form>
</div>
</div>

{{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector:'textarea.bodyfield',
        height:300,
        menubar:false,
    plugins:['link', 'table', 'image', 'autoresize', 'lists'],
    toolbar:'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | table | link image | bullist numlist',
    // content_css:[
    // '{{asset('css/styles.css')}}'
// ]
images_upload_url:'{{route('imageupload')}}', // criar a rota em api.php
image_upload_credentials:true,
convert_urls:false,
});
</script> --}}
@endsection

@section('css')
<link rel="stylesheet" href="/css/admin-styles.css">
@endsection