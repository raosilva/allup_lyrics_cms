@extends('adminlte::page')

@section('title', 'Lista de Músicas')

@section('content_header')
<h1>Listagem de Músicas
    <a href="{{ route('lyrics.create') }}" class="btn btn-sm btn-success ml-3 text-uppercase">Adicionar nova música</a>
</h1>
@endsection

@section('content')

<div class="card">
    <div class="card-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th width="50">ID</th>
                    <th>Artista</th>
                    <th>Título</th>
                    <th>Extra Info</th>
                    <th>URL da Música</th>
                    <th width="200">Ações</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($lyrics as $lyric)
                <tr>
                    <td>{{ $lyric->id }}</td>
                    <td>Nome do Artista</td>
                    <td>{{ $lyric->title }}</td>
                    <td>{{ $lyric->info }}</td>
                    <td>{{ $lyric->video_url }}</td>
                    <td>
                        <a href="{{ route('lyrics.show', ['lyric' => $lyric->id]) }}" target="_blank"
                            class="btn btn-sm btn-success">Ver</a>

                        <a href="{{ route('lyrics.edit', ['lyric' => $lyric->id]) }}"
                            class="btn btn-sm btn-info">Editar</a>

                        <form action="{{ route('lyrics.destroy', ['lyric' => $lyric->id]) }}" method="POST"
                            class="d-inline" onsubmit="return confirm('Tem certeza que deseja excluir este usuário?')">
                            @method('DELETE')
                            @csrf
                            <input type="submit" value="Excluir" class="btn btn-sm btn-danger">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
{{ $lyrics->links() }}
@endsection