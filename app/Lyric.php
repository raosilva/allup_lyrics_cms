<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cohensive\Embed\Facades\Embed;

class Lyric extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $fillable = ['user_id', 'singer_id','title', 'info', 'video_url', 'lyric'];

    public function singer()
    {
        return $this->belongsTo(Singer::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // public function getVideoHtmlAttribute()
    // {
    //     $embed = Embed::make($this->video_url)->parseUrl();
        
    //     return $embed->getHtml();
    // }
}
