<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Singer extends Model
{
    protected $dateFormat = 'd-m-Y H-i-s';
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function lyrics()
    {
        return $this->hasMany(Lyric::class)->withTimestamps();
    }
}
