<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lyric;
use App\Page;
use App\User;
use App\Visitor;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $visitsCount = 0;
        $onlineCount = 0;
        $lyricsCount = 0;
        $userCount = 0;
        $interval = intval($request->input('interval', 30));

        if ($interval > 120) {
            $interval = 120;
        }

        // Contagem de Acessos Únicos

        // $visitsList = Visitor::all();
        // $visitsFilter = $visitsList->unique('ip');
        // $visitsFilter->values()->all();
        // $visitsCount = count($visitsFilter);

        $dateInterval = date('Y-m-d H:i:s', strtotime('-' . $interval . ' days'));
        $visitsCount = Visitor::where('date_access', '>=', $dateInterval)->count();

        // Contagem de Usuários Online

        $datelimit = date('Y-m-d H:i:s', strtotime('-5 minutes'));
        $onlineList = Visitor::select('ip')->where('date_access', '>=', $datelimit)->groupBy('ip')->get();
        $onlineCount = count($onlineList);

        // Contagem de Músicas

        $lyricsCount = Lyric::count();

        // Contagem Total de Usuários

        $userCount = User::count();

        // Gráfico de "Torta"

        $pagePie = [
            'Teste 1' => 100,
            'Teste 2' => 200,
            'Teste 3' => 300
        ];

        // Contagem para o pagePie

        $visitsAll = Visitor::selectRaw('lyrics, count(lyrics) as c')->where('date_access', '>=', $datelimit)->groupBy('lyrics')->get();
        foreach ($visitsAll as $visit) {
            $pagePie[$visit['lyrics']] = intval($visit['c']);
        }

        $pageLabels = json_encode(array_keys($pagePie));
        $pageValues = json_encode(array_values($pagePie));

        return view('admin.home', [
            'visitsCount' => $visitsCount,
            'onlineCount' => $onlineCount,
            'lyricsCount' => $lyricsCount,
            'userCount' => $userCount,
            'pageLabels' => $pageLabels,
            'pageValues' => $pageValues,
            'dateInterval' => $interval
        ]);
    }
}