<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Singer;
use App\Lyric;
use Illuminate\Http\Request;

class LyricController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('can:admin-content');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lyrics = Lyric::orderBy('lyric', 'ASC')->paginate('10');

        return view('admin.lyrics.index', [
            'lyrics' => $lyrics,
        ]);
    }

    /**
     * Show the form for creating a new resource
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // load the create form (app/views/lyrics/create.blade.php)
        return view('admin.lyrics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'title',
            // 'artist',
            'info',
            'video_url',
            'lyric'
        ]);

        $data['slug'] = Str::slug($data['title'], '-');

        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:100'],
            'slug' => ['required', 'string', 'max:100', 'unique:lyrics'],
            // 'artist' => ['required', 'string', 'max:200'],
            'info' => ['string', 'max:100'],
            'video_url' => ['required', 'string', 'max:100', 'unique:lyrics'],
            'lyric' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return redirect()->route('lyrics.create')
                ->withErrors($validator)
                ->withInput();
        }

        // if(strlen($data['video_url']) > 11)
        // {
        //     if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['video_url'], $match))
        //     {
        //         return $match[1];
        //     }
        //     else
        //         return false;
        // }

        // return $data['video_url'];

        $lyric = new Lyric;
        $lyric->title = trim($data['title']);
        $lyric->slug = $data['slug'];
        $lyric->info = $data['info'];
        $lyric->video_url = substr($data['video_url'], -11);
        $lyric->lyric = $data['lyric'];
        $lyric->save();

        Session::flash('message', 'Música adicionada com sucesso!');
        return redirect()->route('lyrics.index');
    }

    /**
     * Display the specified resource
     *
     *
     .
     *
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lyric = Lyric::find($id);

        return view('admin.lyrics.show', [
            'lyric' => $lyric
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lyric = Lyric::find($id);

        if ($lyric) {
            return view('admin.lyrics.edit', [
                'lyric' => $lyric
            ]);
        }

        return redirect()->route('lyrics.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lyric = Lyric::find($id);

        if ($lyric) {
            $data = $request->only([
                'title',
                // 'artist',
                'info',
                'video_url',
                'lyric'
            ]);

            if ($lyric['title'] !== $data['title']) {
                $data['slug'] = Str::slug($data['title'], '-');

                $validator = Validator::make($data, [
                    'title' => ['required', 'string', 'max:100'],
                    'info' => ['string', 'max:100'],
                    'video_url' => ['required', 'string', 'max:100', 'url'],
                    'slug' => ['required', 'string', 'max:100', 'unique:lyrics'],
                    'lyric' => ['string'],
                ]);
            } else {
                $validator = Validator::make($data, [
                    'title' => ['required', 'string', 'max:100'],
                    'info' => ['string', 'max:100'],
                    'video_url' => ['required', 'string', 'max:100', 'url'],
                    'lyric' => ['string'],
                ]);
            }

            if ($validator->fails()) {
                return redirect()->route('lyrics.edit', [
                    'lyric' => $id
                ])
                    ->withErrors($validator)
                    ->withInput();
            }

            $lyric->title = trim($data['title']);
            $lyric->info = $data['info'];
            $lyric->video_url = $data['video_url'];
            $lyric->lyric = $data['lyric'];

            if (!empty($data['slug'])) {
                $lyric->slug = $data['slug'];
            }

            $lyric->save();
        }

        Session::flash('message', 'Música alterada com sucesso!');
        return redirect()->route('lyrics.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lyric  $lyric
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lyric = Lyric::find($id);
        $lyric->delete();

        Session::flash('message', 'Música excluída com sucesso!');
        return redirect()->route('lyrics.index');
    }
}
