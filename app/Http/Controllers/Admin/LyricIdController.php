<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Singer;
use App\Lyric;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LyricIdController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $user_id = Auth::user()->id;

        // $lyrics = Lyric::where('user_id','=',$user_id)->orderBy('lyric', 'ASC')->paginate('10');

        $logged_user = auth()->user();

        if(isset($logged_user)){
            $lyrics = Lyric::where('user_id',$logged_user->id)->orderBy('lyric', 'ASC')->paginate('10');
            //do whatever you need with lyrics

            return view('admin.userlyrics.index', [
            'lyrics' => $lyrics,
            ]); 
        }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.userlyrics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'title',
            // 'artist',
            'info',
            'video_url',
            'lyric'
        ]);

        $data['slug'] = Str::slug($data['title'], '-');

        $validator = Validator::make($data, [
            'title' => ['required', 'string', 'max:100'],
            'slug' => ['required', 'string', 'max:100', 'unique:lyrics'],
            // 'artist' => ['required', 'string', 'max:200'],
            'info' => ['string', 'max:100'],
            'video_url' => ['required', 'string', 'max:100', 'unique:lyrics'],
            'lyric' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            return redirect()->route('authlyrics.create')
                ->withErrors($validator)
                ->withInput();
        }

        // $artist = new Singer;
        // $artist->artist = $data['artist'];
        // $artist->save();

        $lyric = new Lyric;
        $lyric->title = trim($data['title']);
        $lyric->slug = $data['slug'];
        $lyric->info = $data['info'];
        $lyric->video_url = $data['video_url'];
        $lyric->lyric = $data['lyric'];
        $lyric->save();

        Session::flash('message', 'Música adicionada com sucesso!');
        return redirect()->route('authlyrics.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lyric = Lyric::find($id);

        return view('admin.userlyrics.show', [
        'lyric' => $lyric
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lyric = Lyric::find($id);

        if ($lyric) {
            return view('admin.userlyrics.edit', [
                'lyric' => $lyric
            ]);
        }

        return redirect()->route('authlyrics.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lyric = Lyric::find($id);

        if ($lyric) {
            $data = $request->only([
                'title',
                // 'artist',
                'info',
                'video_url',
                'lyric'
            ]);

            if ($lyric['title'] !== $data['title']) {
                $data['slug'] = Str::slug($data['title'], '-');

                $validator = Validator::make($data, [
                    'title' => ['required', 'string', 'max:100'],
                    'info' => ['string', 'max:100'],
                    'video_url' => ['required', 'string', 'max:100', 'url'],
                    'slug' => ['required', 'string', 'max:100', 'unique:lyrics'],
                    'lyric' => ['string'],
                ]);
            } else {
                $validator = Validator::make($data, [
                    'title' => ['required', 'string', 'max:100'],
                    'info' => ['string', 'max:100'],
                    'video_url' => ['required', 'string', 'max:100', 'url'],
                    'lyric' => ['string'],
                ]);
            }

            if ($validator->fails()) {
                return redirect()->route('authlyrics.edit', [
                    'lyric' => $id
                ])
                    ->withErrors($validator)
                    ->withInput();
            }

            $lyric->title = trim($data['title']);
            $lyric->info = $data['info'];
            $lyric->video_url = $data['video_url'];
            $lyric->lyric = $data['lyric'];

            if (!empty($data['slug'])) {
                $lyric->slug = $data['slug'];
            }

            $lyric->save();
        }

        Session::flash('message', 'Música alterada com sucesso!');
        return redirect()->route('authlyrics.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lyric = Lyric::find($id);
        $lyric->delete();

        Session::flash('message', 'Música excluída com sucesso!');
        return redirect()->route('authlyrics.index');
    }
}
